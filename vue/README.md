# FileUploadToCloud


This code contains everything you need to store an image into a Cloudinary account 

Before you use it:

    1. You must sign up for a free Cloudinary account: www.cloudinary.com 
       No credit card is needed to open a free account. 
       There is a limit to the number of Cloudinary credits you can use in a month on the free account.
       The max is 25 credits on the free plan. 
       1 Credit = 1,000 Transformations OR 1 GB Storage OR 1 GB Bandwidth.  
       You should be able to get through the capstone with a free account. 

    2. Note or copy somewhere the following information from your Cloudinary account Dashboard:
        a. Your cloud-name. (you will use this in the URL when loading images)
        b. Your Cloudinary API key (You will specify this in form data you send to Cloudinary)
        c. The Secure Delivery URL sample.  You will find this if you more... in the Dashboard
           and then the down arrow next to the Secure Delivery URL shown it should look 
           something like this: https://res.cloudinary.com/your-cloud-name/image/upload/sample.jpg

    3. You need to generate an upload preset in Cloudinary in order to upload an image.
        To generate an upload preset:
            a. Login to your Cloudinary account. 
            b. Click on the settings icon in the right side of the page.
            c. Click the "Upload" choice on the nav bar in the settings page
            d. Scroll down until you see the "Upload Presets" section on the left.
            e. In the about the middle of the screen click the "Add Upload Presets" link
            f. In the "Add Upload Presets" page:<template>
                  i. Note / Copy the upload preset name that is generated (you will need it!)
                 ii. Change the "Signing mode" to "unsigned" using the drop-down
                iii. Put the name of the folder in Cloudinary you want your images stored
                        if the folder does not exist it will create it for you
                 iv. Click the "Save" button on the top of the screen

    4. You will need a plug-in called Vue-DropZone.  You can get it at: 
            https://rowanwins.github.io/vue-dropzone/docs/dist/#/installation
 
        Vue-DropZone gives you instant drag and drop and data uploads. 
        This is very difficult to do on your own.
        Better to let it be done by a third party library that integrates with Cloudinary.


  The images are actually stored on an image hosting service (e.g. Cloudinary).

  This component posts the image to Cloudinary using a 3rd party called dropzone.

  The sharePhoto() method POSTS a new Post with the hosted imageUrl and the caption that the user provides.

  The URL of the image in Cloudinary is returned from sharePhoto().
   
  Save this URL and use it when you want to reference the image.
    There are several TODO notations in the code indicating where you need to make changes


